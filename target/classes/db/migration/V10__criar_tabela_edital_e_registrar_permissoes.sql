CREATE TABLE edital (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    ano INT NOT NULL,
	processo VARCHAR(30) NOT NULL,
    objeto VARCHAR(5000),
	data_abertura DATE,
	codigo_cidade BIGINT(20) NOT NULL,
	id_concorrente BIGINT(20),
	FOREIGN KEY (codigo_cidade) REFERENCES cidade(codigo),
	FOREIGN KEY (id_concorrente) REFERENCES concorrente(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	
-- Novas permissoes
INSERT INTO permissao (codigo, descricao) values (15, 'ROLE_CADASTRAR_EDITAL');
INSERT INTO permissao (codigo, descricao) values (16, 'ROLE_REMOVER_EDITAL');
INSERT INTO permissao (codigo, descricao) values (17, 'ROLE_PESQUISAR_EDITAL');

-- admin
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 15);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 16);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 17);
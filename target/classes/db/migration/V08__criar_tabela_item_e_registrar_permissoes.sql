CREATE TABLE item (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	descricao VARCHAR(4000) NOT NULL,
	atende BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Novas permissoes
INSERT INTO permissao (codigo, descricao) values (9, 'ROLE_CADASTRAR_ITEM');
INSERT INTO permissao (codigo, descricao) values (10, 'ROLE_REMOVER_ITEM');
INSERT INTO permissao (codigo, descricao) values (11, 'ROLE_PESQUISAR_ITEM');

-- admin
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 9);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 10);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 11);
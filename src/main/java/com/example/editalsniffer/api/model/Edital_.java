package com.example.editalsniffer.api.model;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Edital.class)
public abstract class Edital_ {

	public static volatile SingularAttribute<Edital, Cidade> cidade;
	public static volatile SingularAttribute<Edital, Integer> ano;
	public static volatile SingularAttribute<Edital, String> anexo;
	public static volatile SingularAttribute<Edital, String> processo;
	public static volatile SingularAttribute<Edital, String> objeto;
	public static volatile SingularAttribute<Edital, Long> id;
	public static volatile SingularAttribute<Edital, LocalDate> dataAbertura;
	public static volatile SingularAttribute<Edital, Concorrente> concorrente;

}


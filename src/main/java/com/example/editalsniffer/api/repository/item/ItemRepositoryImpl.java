package com.example.editalsniffer.api.repository.item;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.example.editalsniffer.api.model.Item;
import com.example.editalsniffer.api.model.Item_;
import com.example.editalsniffer.api.repository.RepositoryImpl;

	public class ItemRepositoryImpl extends RepositoryImpl implements ItemRepositoryQuery  {
		
		@PersistenceContext
		private EntityManager manager;	
		
		public List<Item> findByIds(List<String> ids){
			
			CriteriaBuilder builder = manager.getCriteriaBuilder();
			CriteriaQuery<Item> criteria = builder.createQuery(Item.class);
			Root<Item> root = criteria.from(Item.class);

			In<Long> inIds = builder.in(root.get(Item_.id));
			if (ids.isEmpty()) {
				inIds.value((Long) null);
			} else {
				for (String id : ids) {
					inIds.value(Long.parseLong(id));
				}
			}
			criteria.where(inIds);
			TypedQuery<Item> query = manager.createQuery(criteria);
			query.setHint("org.hibernate.cacheable", Boolean.TRUE);

			return query.getResultList();

		}
}

package com.example.editalsniffer.api.repository.edital;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.editalsniffer.api.model.Edital;
import com.example.editalsniffer.api.repository.filter.EditalFilter;

public interface EditalRepositoryQuery {
	
	public Page<Edital> filtrar(EditalFilter editalFilter, Pageable pageable);

}

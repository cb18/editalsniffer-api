package com.example.editalsniffer.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.editalsniffer.api.model.Categoria;
import com.example.editalsniffer.api.repository.categoria.CategoriaRepositoryQuery;

public interface CategoriaRepository extends JpaRepository<Categoria, Long>, CategoriaRepositoryQuery {

}

package com.example.editalsniffer.api.repository.concorrente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.editalsniffer.api.model.Concorrente;
import com.example.editalsniffer.api.repository.filter.ConcorrenteFilter;

public interface ConcorrenteRepositoryQuery {
	
	public Page<Concorrente> filtrar(ConcorrenteFilter concorrenteFilter, Pageable pageable);

}

package com.example.editalsniffer.api.repository.listener;

import javax.persistence.PostLoad;

import org.springframework.util.StringUtils;

import com.example.editalsniffer.api.EditalsnifferApiApplication;
import com.example.editalsniffer.api.model.Edital;
import com.example.editalsniffer.api.storage.S3;

public class EditalAnexoListener {
	
	@PostLoad
	public void postLoad(Edital edital) {
		if (StringUtils.hasText(edital.getAnexo())) {
			S3 s3 = EditalsnifferApiApplication.getBean(S3.class);
			edital.setUrlAnexo(s3.configurarUrl(edital.getAnexo()));
		}
	}

}

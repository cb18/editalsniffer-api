package com.example.editalsniffer.api.repository.filter;

public class EditalFilter {

	private int ano;
	
	private String processo;

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String getProcesso() {
		return processo;
	}

	public void setProcesso(String processo) {
		this.processo = processo;
	}
	
	
	
}

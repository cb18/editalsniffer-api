package com.example.editalsniffer.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.editalsniffer.api.model.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Long> {

}

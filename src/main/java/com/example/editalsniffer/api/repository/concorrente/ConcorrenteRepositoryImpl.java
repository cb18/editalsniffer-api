package com.example.editalsniffer.api.repository.concorrente;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.example.editalsniffer.api.model.Concorrente;
import com.example.editalsniffer.api.model.Concorrente_;
import com.example.editalsniffer.api.model.Lancamento_;
import com.example.editalsniffer.api.repository.RepositoryImpl;
import com.example.editalsniffer.api.repository.filter.ConcorrenteFilter;

public class ConcorrenteRepositoryImpl extends RepositoryImpl implements ConcorrenteRepositoryQuery  {
	
	@PersistenceContext
	private EntityManager manager;	

	@Override
	public Page<Concorrente> filtrar(ConcorrenteFilter concorrenteFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Concorrente> criteria = builder.createQuery(Concorrente.class);
		Root<Concorrente> root = criteria.from(Concorrente.class);
		
		Predicate[] predicates = criarRestricoes(concorrenteFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Concorrente> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(concorrenteFilter));
	}
	
	private Predicate[] criarRestricoes(ConcorrenteFilter concorrenteFilter, CriteriaBuilder builder,
			Root<Concorrente> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(concorrenteFilter.getNome())) {
			predicates.add(builder.like(
					builder.lower(root.get(Concorrente_.nome)), "%" + concorrenteFilter.getNome().toLowerCase() + "%"));
		}
		
		/*builder.equal(root.get(Concorrente_.nome), concorrenteFilter.getNome()));*/
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private Long total(ConcorrenteFilter concorrenteFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Concorrente> root = criteria.from(Concorrente.class);
		
		Predicate[] predicates = criarRestricoes(concorrenteFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}

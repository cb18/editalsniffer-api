package com.example.editalsniffer.api.repository.item;

import java.util.List;

import com.example.editalsniffer.api.model.Item;

public interface ItemRepositoryQuery {
	
	public List<Item> findByIds(List<String> ids);

}

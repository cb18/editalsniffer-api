package com.example.editalsniffer.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.editalsniffer.api.model.Concorrente;
import com.example.editalsniffer.api.repository.concorrente.ConcorrenteRepositoryQuery;

public interface ConcorrenteRepository extends JpaRepository<Concorrente, Long>, ConcorrenteRepositoryQuery {

}

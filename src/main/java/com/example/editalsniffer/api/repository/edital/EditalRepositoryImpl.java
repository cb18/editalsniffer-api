package com.example.editalsniffer.api.repository.edital;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.example.editalsniffer.api.model.Edital;
import com.example.editalsniffer.api.model.Edital_;
import com.example.editalsniffer.api.repository.RepositoryImpl;
// import com.example.editalsniffer.api.model.Edital_;
import com.example.editalsniffer.api.repository.filter.EditalFilter;

public class EditalRepositoryImpl extends RepositoryImpl implements EditalRepositoryQuery  {
	
	@PersistenceContext
	private EntityManager manager;	

	@Override
	public Page<Edital> filtrar(EditalFilter editalFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Edital> criteria = builder.createQuery(Edital.class);
		Root<Edital> root = criteria.from(Edital.class);
		
		Predicate[] predicates = criarRestricoes(editalFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Edital> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(editalFilter));
	}
	
	private Predicate[] criarRestricoes(EditalFilter editalFilter, CriteriaBuilder builder,
			Root<Edital> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (editalFilter.getAno() != 0) {
			predicates.add(builder.equal(root.get(Edital_.ano), editalFilter.getAno()));
		}
		
		if (!StringUtils.isEmpty(editalFilter.getProcesso())) {
			predicates.add(
					builder.equal(root.get(Edital_.processo), editalFilter.getProcesso()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private Long total(EditalFilter editalFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Edital> root = criteria.from(Edital.class);
		
		Predicate[] predicates = criarRestricoes(editalFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}

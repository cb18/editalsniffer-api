package com.example.editalsniffer.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.editalsniffer.api.model.Item;
import com.example.editalsniffer.api.repository.item.ItemRepositoryQuery;

public interface ItemRepository extends JpaRepository<Item, Long>, ItemRepositoryQuery {
	
	public Page<Item> findByDescricaoContaining(String nome, Pageable pageable);
	
	public List<Item> findByDescricaoContainingIgnoreCase(String nome);
	
	

}

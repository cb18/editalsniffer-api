package com.example.editalsniffer.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.editalsniffer.api.model.Edital;
import com.example.editalsniffer.api.repository.edital.EditalRepositoryQuery;

public interface EditalRepository extends JpaRepository<Edital, Long>, EditalRepositoryQuery {

}

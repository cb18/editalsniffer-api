package com.example.editalsniffer.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.editalsniffer.api.model.Concorrente;
import com.example.editalsniffer.api.repository.ConcorrenteRepository;

@Service
public class ConcorrenteService {
	
	@Autowired
	private ConcorrenteRepository concorrenteRepository;	
	
	private Concorrente buscarConcorrentePeloId(Long id) {
		Optional<Concorrente> concorrenteSalvo = concorrenteRepository.findById(id);
		if (!concorrenteSalvo.isPresent()) {
			throw new IllegalArgumentException();
		}
		return concorrenteSalvo.get();
	}	
	
	public Concorrente atualizar(Long codigo, Concorrente concorrente) {
		Concorrente concorrenteSalvo = buscarConcorrentePeloId(codigo);
		
		BeanUtils.copyProperties(concorrente, concorrenteSalvo, "id");

		return concorrenteRepository.save(concorrenteSalvo);
	}

}

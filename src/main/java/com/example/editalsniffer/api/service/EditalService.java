package com.example.editalsniffer.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.editalsniffer.api.model.Edital;
import com.example.editalsniffer.api.repository.EditalRepository;
import com.example.editalsniffer.api.storage.S3;

@Service
public class EditalService {
	
	@Autowired
	private EditalRepository editalRepository;
	
	@Autowired
	private S3 s3;	
	
	public Edital salvar(Edital edital) {
		
		if (StringUtils.hasText(edital.getAnexo())) {
			s3.salvar(edital.getAnexo());
		}
		
		limparReferenciaInvalidaConcorrencia(edital);
		return editalRepository.save(edital);
	}

	
	public Edital atualizar(Long id, Edital edital) {
		Edital editalSalvo = buscarEditalPeloId(id);
		
		if (StringUtils.isEmpty(edital.getAnexo())
				&& StringUtils.hasText(editalSalvo.getAnexo())) {
			s3.remover(editalSalvo.getAnexo());
		} else if (StringUtils.hasText(edital.getAnexo())
				&& !edital.getAnexo().equals(editalSalvo.getAnexo())) {
			s3.substituir(editalSalvo.getAnexo(), edital.getAnexo());
		}

		BeanUtils.copyProperties(edital, editalSalvo, "id");

		limparReferenciaInvalidaConcorrencia(editalSalvo);		
		return editalRepository.save(editalSalvo);
	}
	
	private Edital buscarEditalPeloId(Long id) {
		Optional<Edital> editalSalvo = editalRepository.findById(id);
		if (!editalSalvo.isPresent()) {
			throw new IllegalArgumentException();
		}
		return editalSalvo.get();
	}
	
	private void limparReferenciaInvalidaConcorrencia(Edital edital) {
		/*Se for enviado uma instância da categoria com o código nulo, essa instância será limpada*/
		if (edital.getConcorrente() != null) {
			if (edital.getConcorrente().getId() == null) {
				edital.setConcorrente(null);
			}
		}
	}

}

package com.example.editalsniffer.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.editalsniffer.api.model.Categoria;
import com.example.editalsniffer.api.repository.CategoriaRepository;

@Service
public class CategoriaService {
	
	@Autowired
	private CategoriaRepository categoriaRepository;	
	
	private Categoria buscarCategoriaPeloId(Long id) {
		Optional<Categoria> categoriaSalvo = categoriaRepository.findById(id);
		if (!categoriaSalvo.isPresent()) {
			throw new IllegalArgumentException();
		}
		return categoriaSalvo.get();
	}	
	
	public Categoria atualizar(Long codigo, Categoria categoria) {
		Categoria categoriaSalvo = buscarCategoriaPeloId(codigo);
		
		BeanUtils.copyProperties(categoria, categoriaSalvo, "codigo");

		return categoriaRepository.save(categoriaSalvo);
	}
}

package com.example.editalsniffer.api.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.editalsniffer.api.model.Item;
import com.example.editalsniffer.api.repository.ItemRepository;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepository itemRepository;
	
	public Item salvar(Item item) {
		return itemRepository.save(item);
	}
	
	public List<Item> salvarLista(List<Item> itens) {
		// itens.forEach(i -> salvar(i));
		
		List<Item> listaItens = new ArrayList<Item>();
		
		for (int i = 0; i < itens.size(); i++) {
			listaItens.add(salvar(itens.get(i)));
		}
		
		return listaItens;
	}
	
	public Item atualizar(Long id, Item item) {
		Item itemSalva = buscarItemPeloId(id);

		BeanUtils.copyProperties(item, itemSalva, "id");
		return itemRepository.save(itemSalva);
	}
	
	public List<Item> atualizarLista(List<Item> itens) {
		List<Item> listaItens = new ArrayList<Item>();
		
		for (int i = 0; i < itens.size(); i++) {
			if (itens.get(i).getId() != null) {
				listaItens.add(atualizar(itens.get(i).getId(), itens.get(i)));
			} else {
				listaItens.add(salvar(itens.get(i)));
			}
			
		}
		
		return listaItens;
	}	

	public void atualizarPropriedadeAtende(Long id, Boolean atende) {
		Item itemSalvo = buscarItemPeloId(id);
		itemSalvo.setAtende(atende);
		itemRepository.save(itemSalvo);
	}
	
	public Item buscarItemPeloId(Long id) {
		Optional<Item> itemSalvo = itemRepository.findById(id);
		if (!itemSalvo.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return itemSalvo.get();
	}

	public List<Item> findByIds(String ids) {
		List<String> listIdsStr = Arrays.asList(ids.split("\\s*,\\s*"));
		
		/*List<Long> listIds = new ArrayList<Long>();
		
		listIds.addAll(listIdsStr.stream().map(Long::valueOf).collect(Collectors.toList()));*/
		
		return itemRepository.findByIds(listIdsStr);
	}
	
	public List<Item> findByDescricaoContainingIgnoreCase(String nome) {
		
		if (nome.equals("")) {
			List<Item> listaItemVazia = new ArrayList<Item>();
			return listaItemVazia;
		}
		
		return itemRepository.findByDescricaoContainingIgnoreCase(nome);
	}
	
}

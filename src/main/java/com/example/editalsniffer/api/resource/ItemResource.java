package com.example.editalsniffer.api.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.editalsniffer.api.event.RecursoCriadoEvent;
import com.example.editalsniffer.api.model.Item;
import com.example.editalsniffer.api.repository.ItemRepository;
import com.example.editalsniffer.api.service.ItemService;

@RestController
@RequestMapping("/itens")
public class ItemResource {
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_ITEM') and #oauth2.hasScope('write')")
	public ResponseEntity<Item> criar(@Valid @RequestBody Item item, HttpServletResponse response) {
		Item itemSalvo = itemService.salvar(item);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, itemSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(itemSalvo);
	}
	
	@PostMapping("/lista")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_ITEM') and #oauth2.hasScope('write')")
	public ResponseEntity<List<Item>> criarLista(@Valid @RequestBody List<Item> itens, HttpServletResponse response) {
		List<Item> itensSalvo = itemService.salvarLista(itens);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, itensSalvo.get(0).getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(itensSalvo);
	}	
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_REMOVER_ITEM') and #oauth2.hasScope('write')")
	public void remover(@PathVariable Long id) {
		itemRepository.deleteById(id);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_ITEM')")
	public ResponseEntity<Item> atualizar(@PathVariable Long id, @Valid @RequestBody Item item) {
		try {
			Item itemSalvo = itemService.atualizar(id, item);
			return ResponseEntity.ok(itemSalvo);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("/lista")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_ITEM')")
	public ResponseEntity<List<Item>> atualizarLista(@Valid @RequestBody List<Item> itens) {
		try {
			List<Item> itensSalvo = itemService.atualizarLista(itens);
			return ResponseEntity.ok(itensSalvo);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}	
	
	@PutMapping("/{id}/atende")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_ITEM') and #oauth2.hasScope('write')")
	public void atualizarPropriedadeAtende(@PathVariable Long id, @RequestBody Boolean atende) {
		itemService.atualizarPropriedadeAtende(id, atende);	
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_ITEM')")
	public Page<Item> pesquisar(@RequestParam(required = false, defaultValue = "%") String descricao, Pageable pageable) {
		return itemRepository.findByDescricaoContaining(descricao, pageable);
	}
	
	@GetMapping("/pesquisa")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_ITEM')")
	public List<Item> pesquisarItemPorDescricap(@RequestParam(required = false, defaultValue = "") String descricao) {
		return itemService.findByDescricaoContainingIgnoreCase(descricao);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_ITEM') and #oauth2.hasScope('read')")
	public ResponseEntity<Item> buscarPeloId(@PathVariable Long id) {
		Optional<Item> item = itemRepository.findById(id);
		return item.isPresent() ? ResponseEntity.ok(item.get()) : ResponseEntity.notFound().build();
	}
	
	@GetMapping("/lista/{ids}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_ITEM') and #oauth2.hasScope('read')")
	public ResponseEntity<List<Item>> buscarPeloIds(@PathVariable String ids) {
		try {
			List<Item> itens = itemService.findByIds(ids);
			return ResponseEntity.ok(itens);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}	

}

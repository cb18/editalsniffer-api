package com.example.editalsniffer.api.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.editalsniffer.api.event.RecursoCriadoEvent;
import com.example.editalsniffer.api.model.Concorrente;
import com.example.editalsniffer.api.repository.ConcorrenteRepository;
import com.example.editalsniffer.api.repository.filter.ConcorrenteFilter;
import com.example.editalsniffer.api.service.ConcorrenteService;

@RestController
@RequestMapping("/concorrentes")
public class ConcorrenteResource {

	@Autowired
	private ConcorrenteRepository concorrenteRepository;
	
	@Autowired
	private ConcorrenteService concorrenteService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CONCORRENTE') and #oauth2.hasScope('read')")
	public Page<Concorrente> pesquisar(ConcorrenteFilter concorrenteFilter, Pageable pageable) {
		return concorrenteRepository.filtrar(concorrenteFilter, pageable);
	}
	
	@GetMapping("/lista")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CONCORRENTE') and #oauth2.hasScope('read')")
	public List<Concorrente> listar() {
		return concorrenteRepository.findAll();
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_CONCORRENTE') and #oauth2.hasScope('write')")
	public ResponseEntity<Concorrente> criar(@Valid @RequestBody Concorrente concorrente, HttpServletResponse response) {
		Concorrente concorrenteSalva = concorrenteRepository.save(concorrente);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, concorrenteSalva.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(concorrenteSalva);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CONCORRENTE') and #oauth2.hasScope('read')")
	public ResponseEntity<Concorrente> buscarPeloCodigo(@PathVariable Long id) {
		 Optional<Concorrente> concorrente = concorrenteRepository.findById(id);
		 return concorrente.isPresent() ? ResponseEntity.ok(concorrente.get()) : ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_REMOVER_CONCORRENTE') and #oauth2.hasScope('write')")
	public void remover(@PathVariable Long id) {
		concorrenteRepository.deleteById(id);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_CONCORRENTE')")
	public ResponseEntity<Concorrente> atualizar(@PathVariable Long id, @Valid @RequestBody Concorrente concorrente) {
		try {
			Concorrente concorrenteSalva = concorrenteService.atualizar(id, concorrente);
			return ResponseEntity.ok(concorrenteSalva);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
}

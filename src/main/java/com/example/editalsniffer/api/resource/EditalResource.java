package com.example.editalsniffer.api.resource;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.editalsniffer.api.dto.Anexo;
import com.example.editalsniffer.api.event.RecursoCriadoEvent;
import com.example.editalsniffer.api.model.Edital;
import com.example.editalsniffer.api.repository.EditalRepository;
import com.example.editalsniffer.api.repository.filter.EditalFilter;
import com.example.editalsniffer.api.service.EditalService;
import com.example.editalsniffer.api.storage.S3;

@RestController
@RequestMapping("/editais")
public class EditalResource {

	
	@Autowired
	private EditalRepository editalRepository;
	
	@Autowired
	private EditalService editalService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private S3 s3;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_EDITAL') and #oauth2.hasScope('read')")
	public Page<Edital> pesquisar(EditalFilter editalFilter, Pageable pageable) {
		return editalRepository.filtrar(editalFilter, pageable);
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_EDITAL') and #oauth2.hasScope('read')")
	public ResponseEntity<Edital> buscarPeloId(@PathVariable Long id) {
		Optional<Edital> edital = editalRepository.findById(id);
		return edital.isPresent() ? ResponseEntity.ok(edital.get()) : ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_EDITAL') and #oauth2.hasScope('write')")
	public ResponseEntity<Edital> criar(@Valid @RequestBody Edital edital, HttpServletResponse response) {
		Edital editalSalvo = editalService.salvar(edital);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, editalSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(editalSalvo);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_REMOVER_EDITAL') and #oauth2.hasScope('write')")
	public void remover(@PathVariable Long id) {
		editalRepository.deleteById(id);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_EDITAL')")
	public ResponseEntity<Edital> atualizar(@PathVariable Long id, @Valid @RequestBody Edital edital) {
		try {
			Edital editalSalvo = editalService.atualizar(id, edital);
			return ResponseEntity.ok(editalSalvo);
		} catch (IllegalArgumentException e) {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping("/anexo")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_EDITAL') and #oauth2.hasScope('write')")
	public Anexo uploadAnexo(@RequestParam MultipartFile anexo) throws IOException {
		String nome = s3.salvarTemporariamente(anexo);
		return new Anexo(nome, s3.configurarUrl(nome));
	}

}

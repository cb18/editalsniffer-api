CREATE TABLE concorrente (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO concorrente (id, nome) VALUES(1, 'Concorrente 1');
INSERT INTO concorrente (id, nome) VALUES(2, 'Concorrente 2');


-- Novas permissoes
INSERT INTO permissao (codigo, descricao) values (12, 'ROLE_CADASTRAR_CONCORRENTE');
INSERT INTO permissao (codigo, descricao) values (13, 'ROLE_REMOVER_CONCORRENTE');
INSERT INTO permissao (codigo, descricao) values (14, 'ROLE_PESQUISAR_CONCORRENTE');

-- admin
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 12);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 13);
INSERT INTO usuario_permissao (codigo_usuario, codigo_permissao) values (1, 14);